

import { Container } from "react-bootstrap";
import Counter from "./components/Counter";
import Header from "./components/Header"
import Register from "./components/register"

function App() {
  return (
    <>
    <Container>
      {/*props:forma que tiene la libreria de pasar elementos a un componente hijo*/ }    
      <Header title="Titulo de Prueba 🎯" likes ={4}/>
      <Counter />
      <Register />

    </Container>
    </>
  );
}

export default App;
