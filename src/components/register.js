
import {useState} from "react";


const Register = () => {

    const initialState = {
        name:"",
        mail:""
    };

  const handlerInput = (e) => {
      setuser({...user,[e.target.name]:e.target.value});
  }

    const [user, setuser] = useState(initialState);

    const handleReset = () => {
        setuser(initialState);
    }

    return ( 
        <>
            <h3>Registro</h3>
            <input type="text" placeholder="nombre" name="name" value={user.name} onChange={handlerInput}/>
            <input type="text" placeholder="correo@example.com" value={user.mail} name="mail" onChange={handlerInput}/>
            <h4>{user.name}{user.mail}</h4>
            <button type="button" onClick={handleReset}>Limpiar</button>
        </>


     );
}
 
export default Register;