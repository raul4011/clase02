import {Row,Col,Button} from "react-bootstrap";
import {useState} from "react";

const Counter = () => {

    const [counter, setcounter] = useState(0);


    const add = () => {
        console.log("object")
        setcounter(counter+1);
    }

    const substract = () => {
        setcounter(counter-1);
    }

    return ( 
        <>
        <Row className="text-center">
            <h3>Mi contador bonito 🍤🍲</h3>

        </Row>
        <Row className="text-center">
            <Col>
                <h4>Contador :{counter}</h4>
                <Button variant={"primary"} onClick={add}>+1</Button>
               
                <Button className={"ml-2"} variant={"primary"} onClick={substract}>-1</Button>
            </Col>
        </Row>
        </>
     );
}
 
export default Counter;
